const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const SpritesmithPlugin = require('webpack-spritesmith');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const webpack = require('webpack');

const dev = process.env.NODE_ENV === "dev";

let cssLoader = [
    {
        loader: 'css-loader',
        options: {importLoaders: 1, minimize: !dev ? true : false}
    },
    {
        loader: 'postcss-loader',
        options: {
            ident: 'postcss',
            plugins: (loader) => [
                require('autoprefixer')({
                    browsers: ['last 2 versions', 'ie > 9']
                }),
            ]
        }
    }
];


let config = {
    entry: {
        app: ['./assets/js/app.js', './assets/css/style.scss']
    },
    watch: dev,
    output: {
        path: path.resolve('./dist'),
        filename: 'app.js',
        publicPath: '/dist/'
    },
    devtool: dev ? "cheap-module-eval-source-map" : false,
    devServer: {
        contentBase: path.resolve('./public')
    },
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['eslint-loader']
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.css$/,
                use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: cssLoader
                }))
            },
            {
                test: /\.scss$/,
                use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [...cssLoader,
                        'sass-loader'
                    ]
                }))
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 8192,
                        name: '[name].[hash].[ext]'
                    },
                },
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: ['file-loader']
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: 'style.css'
        }),
        new SpritesmithPlugin({
            src: {
                cwd: path.resolve(__dirname, 'assets/images/sprite/common'),
                glob: '*.png'
            },
            target: {
                image: path.resolve(__dirname, 'assets/images/sprite.png'),
                css: path.resolve(__dirname, 'assets/css/_sprite.scss')
            },
            retina: '@2x',
            apiOptions: {
                cssImageRef: "~sprite.png"
            },
        }),
        new webpack.HotModuleReplacementPlugin(),
        new StyleLintPlugin({
            configFile: '.stylelintrc',
            context: './assets/css/',
            syntax: 'scss',
            emitErrors: false
        }),
    ]
};

if (!dev) {
    config.plugins.push(new UglifyJSPlugin({
        sourceMap: false,
    }));
    config.plugins.push(new CleanWebpackPlugin(
        ['dist'],
        {
            root: path.resolve('./'),
            dry: false,
            watch: true,
        }
    ));

    config.plugins.push(
        new ImageminPlugin({
            test: path.resolve(__dirname, 'assets/images'),
            quality: '95'
        })
    )
}

module.exports = config;
